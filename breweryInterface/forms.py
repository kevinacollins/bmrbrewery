# import form class from django 
from django import forms 
  
# import GeeksModel from models.py 
from .models import Recipe, Log, Hop, Malt, Tap

# from django.forms import inlineformset_factory
  
# create a ModelForm 
class RecipeForm(forms.ModelForm): 
    # specify the name of model to use 
    class Meta: 
        model = Recipe
        fields = '__all__'

    # batch_id = models.CharField(max_length=15, default="")
    # recipe = models.ForeignKey(Recipe, on_delete=models.SET_NULL, null=True)
    # fg = models.CharField(max_length=5, default="")
    # date = models.DateField(default=datetime.date.today)

class LogForm(forms.ModelForm): 
    # specify the name of model to use 
    class Meta: 
        model = Log
        fields = {'batch_id', 'recipe', 'og', 'fg', 'dateBrewed', 'dateKegged'}
        # widgets = {'recipe': forms.Textarea}
        labels = {
            'batch_id': 'Batch Number',
            'og': 'Post Boil Gravity',
            'fg': 'Final Gravity',
            'dateBrewed': "Date Brewed",
            'dateKegged': 'Date Kegged'
        }

class HopForm(forms.ModelForm):
    class Meta:
        model = Hop
        fields = {'name', 'weight', 'time', 'AA', 'recipe'}

class MaltForm(forms.ModelForm):
    class Meta:
        model = Malt
        fields = {'name', 'weight', 'recipe'}

class TapForm(forms.ModelForm):
    class Meta:
        model =  Tap
        fields = '__all__'