from django.http import HttpResponse
from . models import Tap
from . import views
from django.shortcuts import render, redirect

from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from breweryInterface.models import Tap, Recipe, Log, Hop, Malt
from breweryInterface.serializers import TapSerializer, RecipeSerializer, LogSerializer
from .forms import RecipeForm, LogForm, HopForm, MaltForm, TapForm

from django.forms.models import modelformset_factory

from django.forms import inlineformset_factory

import plotly.express as px
import pandas as pd


def index(request):
    tap_list = Tap.objects.all()
    tap1 = Tap.objects.get(tap_id=1)
    tap2 = Tap.objects.get(tap_id=2)
    context = {'tap_1': tap1, 'tap_2': tap2}
    return render(request, 'breweryInterface/taps.html', context)

def schedule(request):
    logList = Log.objects.all()
    
    df = pd.DataFrame([])

    for log in logList:
        data = {'Batch':log.batch_id,
                'Start':log.dateBrewed,
                'Finish':log.dateKegged}
        df = df.append(data, ignore_index=True)
    fig = px.timeline(df, x_start="Start", x_end="Finish", y="Batch")
    fig.update_yaxes(autorange="reversed")

    graph = fig.to_html(full_html=False)

    context = {
        'logList': logList,
        'df': df,
        'graph': graph,
        }
    return render(request, "breweryInterface/schedule.html", context)

# Recipes
# def create_recipe(request):
    
#     context ={}   
#     # create object of form 
#     recipe_form = RecipeForm(request.POST or None, request.FILES or None) 
#     hop_formset = HopForm
#     # check if form data is valid 
#     if recipe_form.is_valid(): 
#         # save the form data to model 
#         recipe_form.save() 
  
#     context = {
#         'recipe_form' : recipe_form,
#         'hop_formset': hop_formset,
#     } 
#     return render(request, "breweryInterface/create_recipe.html", context) 

## Logs
def log_list(request):
    logList = Log.objects.all()
    context = {'logList': logList}
    return render(request, "breweryInterface/log_list.html", context)

def log_form(request, id=0):
        # if this is a POST request we need to process the form data
    if request.method == 'POST':
        if id == 0:
        # create a form instance and populate it with data from the request:
            form = LogForm(request.POST)
        else:
            log = Log.objects.get(pk=id)
            form = LogForm(request.POST, instance=log)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
        return HttpResponseRedirect('/breweryInterface/logs')

    # if a GET (or any other method) we'll create a blank form
    else:
        if id==0:            
            form = LogForm()
        else:
            log = Log.objects.get(pk=id)
            form = LogForm(instance=log)

    return render(request, 'breweryInterface/log_form.html', {'form': form})

        
def log_delete(request,id):
    log = Log.objects.get(pk=id)
    log.delete()
    return HttpResponseRedirect('/breweryInterface/logs')

## Recipes
def recipe_list(request):
    recipeList = Recipe.objects.all()
    context = {'recipeList': recipeList}
    return render(request, "breweryInterface/recipe_list.html", context)

def recipe_form(request, id=0):
        # if this is a POST request we need to process the form data
    # HopFormSet = modelformset_factory(HopForm, exclude=())
    context = {}
    HopFormSet = inlineformset_factory(Recipe, Hop, form=HopForm, fields=('name', 'AA', 'weight', 'time'),extra=3)
    MaltFormSet = inlineformset_factory(Recipe, Malt, form=MaltForm, fields=('name', 'weight'),extra=3)
    recipe = Recipe()

    if request.method == 'POST':
        if id == 0:
        # create a form instance and populate it with data from the request:
            form = RecipeForm(request.POST)
            hopformset = HopFormSet(instance=recipe) 
            maltformset = MaltFormSet(instance=recipe)           
        else:
            recipe = Recipe.objects.get(pk=id)
            form = RecipeForm(request.POST, instance=recipe)
            hopformset = HopFormSet(instance=recipe)
            maltformset = MaltFormSet(instance=recipe)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            created_recipe = form.save()
            hopformset = HopFormSet(request.POST, instance=created_recipe)
            maltformset = MaltFormSet(request.POST, instance=created_recipe)
            if maltformset.is_valid() and hopformset.is_valid():
                created_recipe.save()
                maltformset.save()
                hopformset.save()        

        return HttpResponseRedirect('/breweryInterface/recipes')

    # if a GET (or any other method) we'll create a blank form
    else:
        if id==0:            
            form = RecipeForm(instance=recipe)
            hopformset = HopFormSet(instance=recipe)
            maltformset = MaltFormSet(instance=recipe)
        else:
            recipe = Recipe.objects.get(pk=id)
            form = RecipeForm(instance=recipe)
            hopformset = HopFormSet(instance=recipe)
            maltformset = MaltFormSet(instance=recipe)

    # context['form'] = form
    # formset = HopFormSet()
    context = {
        'form': form,
        'hopformset': hopformset,
        'maltformset': maltformset,
    }
    # context['formset'] = formset
    return render(request, 'breweryInterface/recipe_form.html', context)

# def hop_form(request, pk):
#     # recipe = Recipe.objects.get(id=pk)
#     # form = HopForm(initial={'recipe':recipe})
#     # if request.method == 'POST':
#     #     form = HopForm(request.POST)
#     #     if form.is_valid():
#     #         form.save()
#     #         return HttpResponseRedirect('/breweryInterface/recipes')
        
#     context = {'form': form}
#     return render(request, 'breweryInterface/hop_list.html', context)
        
def recipe_delete(request,id):
    recipe = Recipe.objects.get(pk=id)
    recipe.delete()
    return HttpResponseRedirect('/breweryInterface/recipes')

def tap_list(request):
    tapList = Tap.objects.all()
    context = {'tapList': tapList}
    return render(request, "breweryInterface/tap_list.html", context)

def taps_display(request):
    tap1 = Tap.objects.get(tap_id=1)
    tap2 = Tap.objects.get(tap_id=2)
    context = {'tap_1': tap1, 'tap_2': tap2}
    return render(request, 'breweryInterface/taps_display.html', context)

def tap_form(request, id=0):
        # if this is a POST request we need to process the form data
    if request.method == 'POST':
        if id == 0:
        # create a form instance and populate it with data from the request:
            form = TapForm(request.POST)
        else:
            tap = Tap.objects.get(pk=id)
            form = TapForm(request.POST, instance=tap)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
        return HttpResponseRedirect('/breweryInterface/taps')

    # if a GET (or any other method) we'll create a blank form
    else:
        if id==0:            
            form = TapForm()
        else:
            tap = Tap.objects.get(pk=id)
            form = TapForm(instance=tap)

    return render(request, 'breweryInterface/tap_form.html', {'form': form})



####################################################################
#API
###################################################################
@csrf_exempt
def api_tap_list(request):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        taplist = Tap.objects.all()
        serializer = TapSerializer(taplist, many=True)
        response = JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = TapSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        response = JsonResponse(serializer.errors, status=400)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def api_tap_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        tap = Tap.objects.get(pk=pk)
    except Tap.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = TapSerializer(tap)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = TapSerializer(tap, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        tap.delete()
        return HttpResponse(status=204)


# recipe list 
@csrf_exempt
def api_recipe_list(request):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        recipelist = Recipe.objects.all()
        serializer = RecipeSerializer(recipelist, many=True)
        response = JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        print(data)
        serializer = RecipeSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        response = JsonResponse(serializer.errors, status=400)
    response["Access-Control-Allow-Origin"] = "*"

    return response
    # tap_list = Tap.objects.all()
    # tap1 = Tap.objects.get(tap_id=1)
    # tap2 = Tap.objects.get(tap_id=2)
    # context = {'tap_1': tap1, 'tap_2': tap2}
    # return render(request, 'breweryInterface/index.html', context)

@csrf_exempt
def api_recipe_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        recipe = Recipe.objects.get(pk=pk)
    except Recipe.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = RecipeSerializer(recipe)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = RecipeSerializer(recipe, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        recipe.delete()
        return HttpResponse(status=204)

# log list
@csrf_exempt
def api_log_list(request):
    """
    List all code snippets, or create a new snippet.
    """

    if request.method == 'GET':
        loglist = Log.objects.all()
        serializer = LogSerializer(loglist, many=True)
        response = JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = LogSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        response = JsonResponse(serializer.errors, status=400)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def api_log_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        log = Log.objects.get(pk=pk)
    except log.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = LogSerializer(log)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = LogSerializer(log, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        log.delete()
        return HttpResponse(status=204)