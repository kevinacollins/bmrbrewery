from django.contrib import admin

# Register your models here.

from .models import Tap, Recipe, Log, Hop, Malt, Water
admin.site.register(Tap)
admin.site.register(Recipe)
admin.site.register(Log)
admin.site.register(Hop)
admin.site.register(Water)
admin.site.register(Malt)