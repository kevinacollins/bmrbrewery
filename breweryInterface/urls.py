from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    #APIS
    # path('create_recipe/', views.create_recipe),
    path('api/taps/', views.api_tap_list),
    path('api/taps/<int:pk>/', views.api_tap_detail),
    path('api/recipes/', views.api_recipe_list),
    path('api/recipes/<int:pk>/', views.api_recipe_detail),
    path('api/logs/', views.api_log_list),
    path('api/logs/<int:pk>/', views.api_log_detail),
    # path('taps/', views.tap_list),
    path('recipe', views.recipe_form, name='recipe_insert'),
    path('recipes/', views.recipe_list, name='recipe_list'),
    path('recipes/delete/<int:id>/', views.recipe_delete, name='recipe_delete'),
    path('recipe/<int:id>/', views.recipe_form, name='recipe_update'),
    # path('add_hops/<int:id>/', views.hop_form, name='add_hops'),
    path('log', views.log_form, name='log_insert'),
    path('logs/', views.log_list, name='log_list'),
    path('logs/delete/<int:id>/', views.log_delete, name='log_delete'),
    path('log/<int:id>/', views.log_form, name='log_update'),

    path('taps/', views.tap_list, name='tap_list'),
    path('tap/<int:id>/', views.tap_form, name='tap_update'),
    path('tapsdisplay', views.taps_display, name='taps_display'),
    
    path('schedule/', views.schedule, name='schedule'),
    # path('taps/<int:pk>/', views.tap_detail),
    # path('recipes/', views.recipe_list),
   
    # path('logs/', views.log_list),
    # path('logs/<int:pk>/', views.log_detail)
]