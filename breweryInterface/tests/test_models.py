from django.test import TestCase
from breweryInterface.models import Tap, Recipe, Log, Hop, Malt, Water
from decimal import Decimal

class RecipeTestCase(TestCase):
    def setUp(self):
        testRecipe = Recipe.objects.create(name="testRecipe", 
                                style="testStyle",
                                description="testDescription",
                                efficiency=85,
                                pre_boil_volume=6.5,
                                boil_duration=60,
                                attenuation=90)

        Malt.objects.create(name='PILSNER',
                                weight=10,
                                recipe=testRecipe)

        Malt.objects.create(name='TWOROW',
                                weight=3,
                                recipe=testRecipe)

        Hop.objects.create(name="Hop1",
                            weight=.75,
                            time=60,
                            AA = 14.2,
                            recipe = testRecipe)

        Hop.objects.create(name="Hop2",
                            weight=.5,
                            time=30,
                            AA = 3.4,
                            recipe = testRecipe)

    def test_total_malt_weight(self):
        testRecipe = Recipe.objects.get(name="testRecipe")
        self.assertEqual(testRecipe.total_malt_weight, 13)

    def test_strike_water_temp(self):
        testRecipe = Recipe.objects.get(name="testRecipe")
        self.assertEqual(testRecipe.strike_water_temp, 157)


    def test_post_boil_volume(self):
        testRecipe = Recipe.objects.get(name="testRecipe")
        self.assertEqual(testRecipe.post_boil_volume, 5.5)

    def test_pre_boil_gravity(self):
        testRecipe = Recipe.objects.get(name="testRecipe")
        self.assertEqual(testRecipe.pre_boil_gravity, Decimal('1.062'))

    def test_post_boil_gravity(self):
        testRecipe = Recipe.objects.get(name="testRecipe")
        self.assertEqual(testRecipe.post_boil_gravity, 1.073)

    def test_srm(self):
        testRecipe = Recipe.objects.get(name="testRecipe")
        self.assertEqual(testRecipe.srm, 6.4)

    def test_fg(self):
        testRecipe = Recipe.objects.get(name="testRecipe")
        self.assertEqual(testRecipe.fg, 1.007)

    def test_abv(self):
        testRecipe = Recipe.objects.get(name="testRecipe")
        self.assertEqual(testRecipe.abv, 9.1)

    def test_ibu(self):
        testRecipe = Recipe.objects.get(name="testRecipe")
        self.assertEqual(testRecipe.ibu, 30.6)


class MaltTestCase(TestCase):
    def setUp(self):
        testRecipe = Recipe.objects.create(name="testRecipe", 
                                style="testStyle",
                                description="testDescription",
                                efficiency=85,
                                pre_boil_volume=6.5,
                                boil_duration=60)                                
        testMalt1 = Malt.objects.create(name='PILSNER',
                                        weight=10,
                                        recipe=testRecipe)
        testMalt2 = Malt.objects.create(name='TWOROW',
                                        weight=5,
                                        recipe=testRecipe)

    def test_MAXPPG(self):
        testMalt1 = Malt.objects.get(name="PILSNER")
        # print(testMalt1.MAXPPG)
        testMalt2 = Malt.objects.get(name="TWOROW")

    def test_COLOR(self):
        testMalt1 = Malt.objects.get(name="PILSNER")
        # print(testMalt1.MAXPPG)
        testMalt2 = Malt.objects.get(name="TWOROW")

class WaterTestCase(TestCase):
    def setUp(self):
        self.testWater = Water.objects.create(targetCalciumConcentration = 50,
                                                targetMagnesiumConcentration = 7,
                                                targetSulfateConcentration = 75,
                                                targetChlorideConcentration = 60)

    def test_gypsumWght(self):
        self.assertEquals(round(self.testWater.gypsumWght,2),0.26)

    def test_MGSO4Wght(self):
        self.assertEquals(self.testWater.MGSO4Wght, 0.35)

    def test_CaCl2Wght(self):
        self.assertEquals(self.testWater.CaCl2Wght, 0.47)

    def test_calcConc(self):
        self.assertEquals(self.testWater.calcConc, 50.05)

    def test_sulfConc(self):
        self.assertEquals(self.testWater.sulfConc, 75.0)

    def test_chlorConc(self):
        self.assertEquals(self.testWater.chlorConc, 59.97)

    def test_magConc(self):
        self.assertEquals(self.testWater.magConc, 6.87)