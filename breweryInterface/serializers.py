from rest_framework import serializers
from breweryInterface.models import Tap, Recipe, Log
import datetime

class TapSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tap
        fields = ['tap_id', 'beer_name', 'beer_style', 'beer_ABV', 'beer_IBU', 'beer_SRM', 'tap_date', 'beer_description']

class RecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recipe
        fields = ['recipe_id','name']

class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Log
        fields = ['batch_id']