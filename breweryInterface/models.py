from django.db import models
import datetime
from decimal import Decimal
import math
import numpy as np
from scipy.optimize import nnls

# Create your models here.


class Water(models.Model):
    name = models.CharField(max_length=30, default="")
    targetCalciumConcentration = models.DecimalField(max_digits=5, decimal_places=2, default='0')
    targetMagnesiumConcentration = models.DecimalField(max_digits=5, decimal_places=2, default='0')    
    targetSulfateConcentration = models.DecimalField(max_digits=5, decimal_places=2, default='0')
    targetChlorideConcentration = models.DecimalField(max_digits=5, decimal_places=2, default='0') 
  
    # projectedCalciumConcentration = 0;
    # projectedMagnesiumConcentration = 0;
    # projectedSulfateConcentration = 0;
    # projectedChlorideConcentration = 0;
    
    # suggestedGypsum = models.DecimalField(max_digits=5, decimal_places=2, default='0')
    # suggestedMGSO4 = models.DecimalField(max_digits=5, decimal_places=2, default='0')
    # suggestedCaCl2 = models.DecimalField(max_digits=5, decimal_places=2, default='0')

    def __str__(self):
        return str(self.name)

    def calculate(self):
        ratioMatrix = np.array([[61.472, 0, 71.94],[26.06,0,0],[147.47, 102.9,0],[0,0,127.48]])
        targetMatrix = np.array([self.targetCalciumConcentration,self.targetMagnesiumConcentration,self.targetSulfateConcentration,self.targetChlorideConcentration])
        suggestedValues, rnorm = nnls(ratioMatrix,targetMatrix)
        projectedValues = ratioMatrix.dot(suggestedValues)

        return (suggestedValues, projectedValues)
        
    @property
    def gypsumWght(self):
        suggestedValues, projectedValues = self.calculate()
        return round(float(suggestedValues[0]),2)
        
    @property
    def MGSO4Wght(self):
        suggestedValues, projectedValues = self.calculate()
        return round(float(suggestedValues[1]),2)
    
    @property
    def CaCl2Wght(self):
        suggestedValues, projectedValues = self.calculate()
        return round(float(suggestedValues[2]),2)  

    @property
    def calcConc(self):
        suggestedValues, projectedValues = self.calculate()
        return round(projectedValues[0],2)
        
    @property
    def magConc(self):
        suggestedValues, projectedValues = self.calculate()
        return round(projectedValues[1],2)
    
    @property
    def sulfConc(self):
        suggestedValues, projectedValues = self.calculate()
        return round(projectedValues[2],2)  

    @property
    def chlorConc(self):
        suggestedValues, projectedValues = self.calculate()
        return round(projectedValues[3],2) 

class Recipe(models.Model):
    # recipe_id = models.IntegerField(null=True, blank=True)
    name = models.CharField(max_length=30, default="")
    style = models.CharField(max_length=30, default="")
    description = models.CharField(max_length=200, default="")
    efficiency = models.DecimalField(max_digits=5, decimal_places=3, default=70)    
    pre_boil_volume = models.DecimalField(max_digits=5, decimal_places=2, default=6.5)
    # post_boil_volume = models.DecimalField(max_digits=5, decimal_places=2, default=5.5)
    # pre_boil_gravity = models.IntegerField(null=True, blank=True)
    boil_duration = models.IntegerField(null=True, blank=True)
    # abv = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    # ibu = models.IntegerField(null=True, blank=True, default=0)
    # srm = models.IntegerField(null=True, blank=True, default=0)    
    # fg = models.DecimalField(max_digits=5, decimal_places=3, default=0)    
    # post_boil_gravity = models.DecimalField(max_digits=5, decimal_places=3, default=1.000)  
    attenuation = models.IntegerField(null=True, blank=True)
    water = models.ForeignKey(Water, on_delete=models.SET_NULL, null=True)
    mash_temp = models.IntegerField(null=True, blank=True, default=150)
    strike_volume = models.DecimalField(max_digits=5, decimal_places=2, default=7)
    fermentation_temp = models.IntegerField(null=True, blank=True, default = 165)

    def __str__(self):
        return(self.name)

    @property
    def total_malt_weight(self):
        maltset = self.malt_set.all()
        totalWeight = 0
        for malt in maltset:
            totalWeight = totalWeight + malt.weight
        return round(totalWeight,2)

    @property
    def strike_water_temp(self):
        GRAIN_TEMPERATURE = 70
        water_grain_ratio = (self.strike_volume*4)/self.total_malt_weight   
        a = 0.2/float(water_grain_ratio)
        return round((a)*(self.mash_temp-GRAIN_TEMPERATURE)+self.mash_temp,0)

    @property
    def post_boil_volume(self):
        BOILOFFRATE = 1 #gallon per hour
        a = float(self.boil_duration)/float(60)
        b = a*BOILOFFRATE
        c = float(self.pre_boil_volume)-float(b)
        return c
        # return self.pre_boil_volume-((self.boil_duration/60)*BOILOFFRATE)

    @property
    def pre_boil_gravity(self):
        # print(self.hop_set.all())
        # hopset = self.hop_set.all()
        # ibu = 0
        # for hop in hopset:
        #     ibu = ibu + 5
        # return ibu
        maltset = self.malt_set.all()
        totalPPG = 0
        totalWeight = 0
        for malt in maltset:
            totalWeight = totalWeight + malt.weight
            # print('maxppg',malt.MAXPPG)
            # print('weight',malt.weight)
            maltPPG = (malt.MAXPPG*malt.weight*(self.efficiency/100))/self.pre_boil_volume
            totalPPG = totalPPG + maltPPG
        pre_boil_gravity = round(1 + totalPPG/1000,3)
        return pre_boil_gravity

    @property
    def post_boil_gravity(self):
        maltset = self.malt_set.all()
        totalPPG = 0
        totalWeight = 0
        for malt in maltset:
            totalWeight = totalWeight + malt.weight
            # print('maxppg',malt.MAXPPG)
            # print('weight',malt.weight)
            a = malt.MAXPPG*malt.weight
            b = self.efficiency/100
            c = a*b
            d = float(c)/float(self.post_boil_volume)
            maltPPG = d
            totalPPG = totalPPG + maltPPG
        post_boil_gravity = round(1 + totalPPG/1000,3)
        return post_boil_gravity
    
    @property
    def srm(self):
        maltset = self.malt_set.all()
        totalSRM = 0
        for malt in maltset:
            a = float(malt.SRM)*float(malt.weight)
            d = float(a)/float(self.post_boil_volume)
            maltSRM = d
            totalSRM = totalSRM + maltSRM
        SRM = round(totalSRM,1)
        return SRM
    
    @property
    def ibu(self):
        hopset = self.hop_set.all()
        totalIBU = 0
        for hop in hopset:
            AAU = float(hop.AA) * float(hop.weight)
            fg = 1.65*pow((0.000125),(self.post_boil_gravity-1))
            ft = (1-math.exp(-0.04*hop.time))/4.15
            a = fg*ft
            b = (AAU * a * 75)/self.post_boil_volume

            totalIBU = totalIBU + b

        IBU = round(totalIBU,1)
        return IBU
    
    @property
    def fg(self):
        a = (self.post_boil_gravity-1)*1000
        b = (a*(100-self.attenuation)/100)
        c = b/1000 + 1
        d = round(c,3)
        return d

    @property
    def abv(self):
        a = 76.08*(self.post_boil_gravity - self.fg)
        b = 1.775-self.post_boil_gravity
        c = self.fg/0.794
        abv = (float(a)/float(b)) * float(c)

        return round(abv,1)       

    @property
    def gypsumWght(self):
        return round(self.water.gypsumWght*self.post_boil_volume,2)

    @property
    def MGSO4Wght(self):
        return round(self.water.MGSO4Wght*self.post_boil_volume,2)
    
    @property
    def CaCl2Wght(self):
        return round(self.water.CaCl2Wght*self.post_boil_volume,2)

class Hop(models.Model):
    name = models.CharField(max_length=255, default = "")
    weight = models.CharField(max_length=255, default = "")
    time = models.IntegerField(default=60)
    AA = models.DecimalField(max_digits=4, decimal_places=2, default = 0.0)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

class Malt(models.Model):
    MALT_CHOICES = [
        ('ACID', 'Acid'),
        ('AMBER', 'Amber'),
        ('AROMATIC', 'Aromatic'),
        ('SIXROW', '6-Row'),
        ('PILSNER', 'Pilsner'),
        ('TWOROW', '2-Row'),
        ('VICTORY', 'Victory'),
        ('VIENNA', 'Vienna'),
        ('MUNICH', 'Munich'),
    ]
    name = models.CharField(max_length=15, choices=MALT_CHOICES)
    weight = models.DecimalField(max_digits=4, decimal_places=2)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

    @property
    def MAXPPG(self):
        MAXPPGS = {
            'ACID':27,
            'AMBER':35,
            'AROMATIC':36,
            'TWOROW':37,
            "SIXROW":35,
            'VICTORY':25,
            'VIENNA':35,
            'MUNICH':35,
            'PILSNER':36,
        }
        return MAXPPGS.get(self.name)

    @property
    def SRM(self):
        COLORS = {
            'ACID':3,
            'AMBER':22,
            'AROMATIC':26,
            'TWOROW':1.8,
            "SIXROW":1.8,
            'VICTORY':35,
            'VIENNA':4,
            'MUNICH':10,
            'PILSNER':3, 
        }
        return COLORS.get(self.name)

class Log(models.Model):
    batch_id = models.CharField(max_length=15, default="")
    recipe = models.ForeignKey(Recipe, on_delete=models.SET_NULL, null=True)
    og = models.DecimalField(max_digits=5, decimal_places=3, default = 1.050, blank=True)
    fg = models.DecimalField(max_digits=5, decimal_places=2, blank=True, default=1.000)
    dateBrewed = models.DateField(default=datetime.date.today)
    dateKegged = models.DateField(default=datetime.date.today)
    notes = models.CharField(max_length=300, default="")

    def __str__(self):
        return self.batch_id


class Tap(models.Model):
    tap_id = models.IntegerField()
    log = models.ForeignKey(Log, on_delete=models.SET_NULL, null=True)
    # beer_name = models.CharField(max_length=200, default="")
    # beer_style = models.CharField(max_length=120, default="")    
    # beer_ABV = models.FloatField(blank = True, default=0.0)
    # beer_IBU = models.IntegerField(blank = True, default = 0)
    # beer_SRM = models.IntegerField(blank = True, default = 0)
    # tap_date = models.DateField(null = True, blank=True)
    # beer_description = models.CharField(max_length=500, default="")

# class Grain(models.Model):
#     name = models.CharField(max_length=50, default="")
#     max_ppg = models.IntegerField()
# class Hop(models.Model):
#     name = models.CharField(max_length=15, default="")
#     recipe = models.ForeignKey(Recipe, null=True, on_delete=models.SET_NULL)




# class Mash Step




# There's some very good documentation on creating custom fields here.

# However, I think you're overthinking this. It sounds like you actually just want a standard foreign key, but with the additional ability to retrieve all the elements as a single list. So the easiest thing would be to just use a ForeignKey, and define a get_myfield_as_list method on the model:

# class Friends(model.Model):
#     name = models.CharField(max_length=100)
#     my_items = models.ForeignKey(MyModel)

# class MyModel(models.Model):
#     ...

#     def get_my_friends_as_list(self):
#         return ', '.join(self.friends_set.values_list('name', flat=True))

# Now calling get_my_friends_as_list() on an instance of MyModel will return you a list of strings, as required.