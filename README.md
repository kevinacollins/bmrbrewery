# BMRBrewery

BMR Brewery - Brewery Interface app is a django application meant to be a central repository of all brewery opterations data. 

The application is built with Django, and is therefore meant to be served to individual clients. 

# Use
The central idea around the app is to not duplicate information. This is achieved by creating objects, then selecting instances of those objects in new objects.

For example, when I create a brew day log, I am brewing a known recipe. I can brew the same recipe twice, so why duplicate all of this information in both a recipe file and a log file. Instead, you can link the recipe file to the log file. 

# User Stories

As a user, i want to be able to create new recipes
-style
-description
-target_fg
-mash_steps
-target_pre_boil_gravity
-target_post_boil_gravity
-target_efficiency
-target_ibu
-grain_list
(list grain types)
-hop_list
(list hop types)

as a user, i want to be able to use a brewday log to assist in brewing
-show recipe information
-make adjustments

as a user, i want to be able to view past logs
-have a screen that pulls up a latex version of past logs

as a user, i want to be able control the chest freezer
-future release. probably incorporate node-red though

as a user, i want to be able to control the brewery
-future release. probably incorporate node-red though

as a user, i want to be able to see what beers are on tap
-name
-style
-abv
-ibu
-srm
-tap_date
-description
-view the brew log from the tap screen

# Version History
Version 1
-initial version

# Issues List
TODO
change all the api calls to /api/ in the url

Version 1
Create a recipe
Create a taplist


TODO
implement mash mash_steps
implement fermentation steps
sort log  hop additions by time
add foreign key for brewers present
add in smb dosage
calculate strike volume based on mash weight
import/export from json
create yeast db
create water page
incorporate ph readings
add in schedule (gantt chart)